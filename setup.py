from setuptools import setup

setup(name='facebook_updates',
      version='0.1',
      description='Hilo de obtención de actualizaciónes de páginas de facebook',
      author='elShaio',
      author_email='develdkmd@outlook.com',
      url='',
      packages=['facebook_updates'],
      install_requires=[
          'requests',
          'bs4',
          'python-dateutil'
      ])
