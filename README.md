# facebook_updates

## Índice

[TOC]

## Instalación
Para instalarlo solamente es hacerlo mediante git
~~~bash
# Por ssh
pip install git+ssh://git@gitlab.com/shaio/facebook_updates.git

# Por https
pip install git+https://gitlab.com/shaio/facebook_updates.git
~~~

## Uso

Para usarlo, simplemente se hace un
~~~python
from facebook_updates.Updater import Updater
~~~

Y se puede probar con el siguiente ejemplo

~~~python
logging.basicConfig(format='[%(asctime)s] %(levelname)s - %(message)s',
                    level=logging.DEBUG)

idactual = ''


def imprimir(items):
    for item in items:
        print(item)


def esnuevoid(elid):
    return idactual == elid


def guardarid(elid):
    global idactual
    idactual = elid

pagina = 'INAPISinaloa'

hilo = Updater(pagina, notificar=imprimir, verificar=esnuevoid, actualizar=guardarid)

hilo.start()

while True:
    pass

~~~


## Especificaciones
El hilo puede funcionar de manera estable si solamente se le establece el parámetro `pagina` a buscar,  esto sale por ejemplo, si la página a buscar es
`https://es-la.facebook.com/INAPISinaloa/ `
el parámetro a enviar al hilo será `INAPISinaloa` como en el ejemplo de arriba.

El hilo utiliza el módulo `logging` para su salida, si se requiere ver ésta, solamente hay que establecer en nivel a `DEBUG` para poder verlo.

Aunque el hilo funcione de manera independiente sin nada más que la página, es altamente recomendable definir 3 funciones para su funcionamiento. estas son las siguientes:

### Parámetros altamente recomendables

#### notificar
Éste parámetro acepta una función la cual recibe un arreglo de elementos del tipo `facebook_updates.FacebookData`.

Un ejemplo de la función podría ser la siguiente:
~~~python
    def _default_notificar(posts):
        for post in posts:
            logging.debug(post)
~~~

#### verificar y actualizar
Ésta función debe estar en conjunto con actualizar, la idea es tener una variable que esté compartida entre estas dos funciones, para revisar si el último post que obtenga el hilo sea distinto del último que realizó en su búsqueda anterior.

Las funciones deben de recibir un parámetro, el cual será el id que verificará.El valor inicial de ésta variable puede ser vacío.

Un ejemplo de estas funciones puede ser el siguiente:
~~~python
_id = ''

def _default_verificar(id_nuevo):
    return _id == id_nuevo


def _default_actualizar(id_nuevo):
    global _id
    _id = id_nuevo
~~~

### Otros parámetros

#### espera
Establece el tiempo en segundos, flotante, de espera entre peticiones hacia la página de facebook
El default es de `30` segundos. Es recomendable un tiempo alrededor del default para evitar baneos por parte de facebook.

#### nombre
Nombre del hilo a crear, esto puede ser util para la gestión de los hilos por parte de programas, así para organizarlos.
El valor default es el valor `None`.

#### reintentos_maximos
Número de reintentos máximos en dado caso que el scrapper no obtenga información. El default es `5` reintentos.

#### reintentos_maximo_tiempos
Segundos a esperar entre cada reintento.
El default es de `5` segundos.

## Funciones

### join
Como nota, la función del hilo `join` se ha reescrito para que cuando sea llamado, el hilo muera.
