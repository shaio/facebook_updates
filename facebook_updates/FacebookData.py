class FacebookData():

    def __init__(self):
        self.id = None
        self.page_id = None
        self.contenido = None
        self.link = None
        self.link_contenido = None
        self.fecha = None

    def __str__(self):
        return '{} | {} | {} | {} | {} | {}'.format(
            self.id,
            self.page_id,
            self.contenido,
            self.link,
            self.link_contenido,
            self.fecha
        )
