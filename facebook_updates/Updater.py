import threading
import logging
from time import sleep

from facebook_updates import Errors, utilidades
from facebook_updates.FacebookData import FacebookData

import requests


class Updater(threading.Thread):

    def __init__(self, pagina, **kwargs):
        """
        Hilo de ejecución de actualización de estados de facebook
        :param pagina: Página de facebook de la cual se obtendrá información
        :param kwargs: Argumentos varios para actualizar la información
        """
        self._sleepperiod = kwargs.get('espera', 30.0)
        nombre_hilo = kwargs.get('nombre', None)
        self._reintentos = kwargs.get('reintentos_maximos', 5)
        self._reintentos_tiempo = kwargs.get('reintentos_maximo_tiempos', 5)
        self._reintentos_tiempo = self._reintentos_tiempo * 1000

        # funciones
        self._notificar = kwargs.get('notificar', self._default_notificar)
        self._verificar = kwargs.get('verificar', self._default_verificar)
        self._actualizar = kwargs.get('actualizar', self._default_actualizar)

        self.pagina = pagina

        self.url = utilidades.obtener_pagina_facebook(self.pagina)

        self._id = ''
        self._stopevent = threading.Event()
        threading.Thread.__init__(self, name=nombre_hilo)

    def run(self):
        while not self._stopevent.isSet():
            self.obtener_actualizaciones()
            self._stopevent.wait(self._sleepperiod)

    def join(self, timeout=None):
        self._stopevent.set()
        threading.Thread.join(self, timeout)

    def obtener_actualizaciones(self, reintentos=None):
        if reintentos is None:
            reintentos = self._reintentos

        logging.debug('Obtniendo información, intentos restantes: %d', reintentos)

        statuscode = 0
        facebook_page = ''

        try:
            facebook_page = requests.get(self.url)
            statuscode = facebook_page.status_code
        except Exception as e:
            logging.debug('Hubo un error al intentar obtener el html de la página, el error fue: %s', str(e))

        if statuscode != 200 and reintentos != 0:
            logging.debug('No se ha podido obtener información, se obtuvo el statuscode %s, quedan %s reintentos',
                          statuscode, reintentos)
            sleep(self._reintentos_tiempo)
            self.obtener_actualizaciones(reintentos - 1)
        elif statuscode != 200 and reintentos == 0:
            raise Errors.IntentosMaximos(
                'No se ha podido obtener información de la página {} y se agotó el número de intentos máximos'.format(
                    self.pagina))
        else:
            posts = utilidades.obtener_posts(facebook_page.text)

            posts_agregados = []

            for post in posts:
                fbd = FacebookData()

                fb_json_info = utilidades.obtner_json_info(post)

                fbid = utilidades.obtener_post_id(fb_json_info)

                fb_pageid = utilidades.obtener_pageid(fb_json_info)

                contenido = utilidades.obtener_post_contenido(post)

                url = utilidades.obtener_url(post)

                link = utilidades.obtener_url_link(url)

                link_text = utilidades.obtener_url_link_text(url)

                fecha = utilidades.obtener_fecha(fb_json_info)

                fbd.id = fbid
                fbd.page_id = fb_pageid
                fbd.contenido = contenido
                fbd.link_contenido = link_text
                fbd.link = link
                fbd.fecha = fecha

                if self._verificar(fbd.id):
                    break

                posts_agregados.append(fbd)

            if len(posts_agregados) != 0:
                self._actualizar(posts_agregados[0].id)

            if self._notificar is not None:
                self._notificar(posts_agregados)

    @staticmethod
    def _default_notificar(posts):
        for post in posts:
            logging.debug(post)

    def _default_verificar(self, id_nuevo):
        return self._id == id_nuevo

    def _default_actualizar(self, id_nuevo):
        self._id = id_nuevo
