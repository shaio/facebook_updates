from bs4 import BeautifulSoup
import json
from datetime import datetime
from dateutil import tz


def obtener_pagina_facebook(pagina):
    _URLBASE = 'https://mobile.facebook.com/{}/?ref=page_internal&_rdr'
    respuesta = _URLBASE.format(pagina)

    return respuesta


def obtener_posts(pagina):
    soup = BeautifulSoup(pagina, 'html.parser')
    posts = soup.select('div#recent > div > div > div')

    return posts


def obtner_json_info(post):
    return json.loads(post.attrs['data-ft'])


def obtener_post_id(fb_json_info):
    return fb_json_info['mf_story_key']


def obtener_post_contenido(post):
    contenido = post.select_one('div:nth-of-type(1) > div > span > p')

    if contenido is None:
        return ''
    else:
        return contenido.text


def obtener_url(post):
    return post.select_one('div > a')


def obtener_url_link(url):
    link = url.attrs['href']
    link = link.replace('https://lm.facebook.com/l.php?u=', '')
    index_of_suburl = link.find('&h=')
    if index_of_suburl != -1:
        link = link[:index_of_suburl]
        link = link.replace('%3A', ':')
        link = link.replace('%2F', '/')

    return link


def obtener_url_link_text(url):
    return url.text


def obtener_pageid(fb_json_info):
    return fb_json_info['page_id']


def obtener_fecha(fb_json_info):
    from_zone = tz.tzutc()
    to_zone = tz.tzlocal()

    epoch = fb_json_info['page_insights'][fb_json_info['page_id']]['post_context']['publish_time']

    tmp_fecha = datetime.utcfromtimestamp(epoch)

    tmp_fecha = tmp_fecha.replace(tzinfo=from_zone)

    respuesta = tmp_fecha.astimezone(to_zone)

    return respuesta
